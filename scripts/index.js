const workSpeed = [8, 7, 6, 5, 4, 3, 2] //скорость работы участников (поинты). колво чисел = колво участников
const backlog = [1, 2, 3, 4, 5, 6, 7, 8] //задания которые нужно сделать. колчо чисел = колво заданий
const deadline = new Date(2023, 1, 30) //дата дедлайна

const checkComplianceWithDeadlines = (workSpeed, backlog, deadline) => {

    let sumPoints = 0;
    workSpeed.forEach(element => {
        sumPoints += element; //сумма поинтов
    });

    let workPerDay = sumPoints / 8; //количествл работы сделанной за 8 часов (1 рабочий день)

    let sumExercises = 0;
    backlog.forEach(element => {
        sumExercises += element; //сумма заданий
    })

    let workDays = 0; //счетчик будних дней между текущей датой и дедлайном
    let today = new Date();
    while (today < deadline) { 
        if (today.getDay() !== 0 && today.getDay() !== 6) { 
      workDays++;
      }
      
    today.setDate(today.getDate() + 1);
    } 
    
    const daysOfTheWeek = 7;
    const weekdays = 5;
    let amountOfDays = (sumExercises / workPerDay) * (weekdays / daysOfTheWeek); // кол-во необходимых рабочих дней
    let requiredTime = today.setDate(today.getDate() + amountOfDays); // сколько тратят времени
    
   if (requiredTime <= deadline) {
    return `Усі завдання будуть успішно виконані за ${workDays} днів до настання дедлайну!`;
  } else {
    let extraHours = ((requiredTime - deadline) / (1000 * 60 * 60)).toFixed(1);
    return `Команді розробників доведеться витратити додатково ${extraHours} годин після дедлайну, щоб виконати всі завдання в беклозі`;
  }
};

console.log(checkComplianceWithDeadlines(workSpeed, backlog, deadline));
